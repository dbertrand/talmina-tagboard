<?php
//Include core classes
require_once('includes/srp_core.php');

//the user is requesting to shout
if(SRPCore()->url(1) == 'shout')
{
    //get the data and error-check it
    $identifier = md5(rawurldecode($_POST['board']));
    $nick = strip_tags(rawurldecode($_POST['nick']));
    //if the email is blank mark it as blank
    if(!strlen($_POST['email']))
    {
        $email = '';
    }
    else
    {
        $email = md5(strtolower(trim(rawurldecode($_POST['email']))));
    }
    //strip all but necessary tags
    $body = strip_tags(rawurldecode($_POST['body']), '<b><i><strong><em><u><strike><del><div><br>');


    //check body string length
    //if shout is over 255 chars then, we need to use the continued table


    SRPCore()->query(
        "INSERT INTO posts
        (board_id, nick, email, `text`)
        VALUES
        ((SELECT board_id FROM boards WHERE MD5(identifier) = '$identifier'), '$nick', '$email', '$body')"
    );
    
}
//the user is polling for new posts
else if(SRPCore()->url(1) == 'poll')
{
    //get the board we are on, and the last post id
    //post ids are incremented, so newer posts will have a larger id
    $last_id = intval($_POST['last_id']);
    $identifier = md5(rawurldecode($_POST['board']));

    //get all new posts from the board
    //limit to 50
    $post_res = SRPCore()->query(
        "SELECT * FROM posts WHERE board_id = (SELECT board_id FROM boards WHERE MD5(identifier) = '$identifier') AND post_id > $last_id ORDER BY posted LIMIT 50"
    );

    //buffer the posts
    ob_start();
    while($post = $post_res->fetch())
    {
        //if the email is not blank do a gravatar post
        if($post['email'] != '') {
    ?>
        <!-- an individual item "Tag Board Post" -->
        <div class="talmina_tbp">
            <!-- the avvy, date, other info -->
            <div class="talmina_tbp_avvy">
                <img src="http://www.gravatar.com/avatar/<?php echo $post['email']; ?>" />
                <?php echo date('Y-m-d H:i', strtotime($post['posted'])); ?>
            </div>
            <div class="talmina_tbp_body">
                <span class="talmina_tbp_nick"><?php echo $post['nick']; ?>:</span>
                <?php echo $post['text']; ?>
            </div>
            <div class="talmina_tbp_clear"></div>
        </div>
        <?php
        //there is no email, so do a full-wide post
        } else { ?>
        <!-- One that has no avvy -->
        <div class="talmina_tbp talmina_tbp_full">
            <div class="talmina_tbp_body">
                <span class="talmina_tbp_nick"><?php echo $post['nick']; ?>:</span>
                <?php echo $post['text']; ?>
                <span class="talmina_tbp_date"><?php echo date('Y-m-d H:i', strtotime($post['posted'])); ?></span>
            </div>
            <div class="talmina_tbp_clear"></div>
        </div>
    <?php
    //set the last id so that we know where to continue from
	$last_id = $post['post_id'];
        }
    }
    $post_text = ob_get_clean();

    $data = array( 'post_text' => $post_text, 'last_id' => $last_id);
    echo json_encode($data, JSON_HEX_APOS | JSON_UNESCAPED_UNICODE);
}
else if(SRPCore()->url(1) == 'board')
{
    //get the board
    $board = SRPCore()->query('SELECT * FROM boards WHERE identifier = \''.SRPCore()->url(2, SRP_ESC_SQL).'\' LIMIT 1');
    
    // if we don't have a board return a 404 and die
    if($board->num_rows() != 1)
    {
        //not found header
        header('HTTP/1.0 404 Not Found');
        ?>
        <h1>Sorry</h1>
        That resource was not found.
        <?php
        die;
    }

    //otherwise we are good! so fetch the board
    $board = $board->fetch();
?>
<html>
<head>
<!-- set the base url -->
<base href="http://talmina.org/tagboard/" />
<!-- Talmina TB uses jQuery (https://github.com/jquery/jquery), which is graciously hosted by google. -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<!-- uses an async timer written for jQuery (http://jchavannes.com/jquery-timer) -->
<script src="res/jquery.timer.js"></script>
<!-- Include font-awesome for Icons (http://fortawesome.github.com/Font-Awesome/) -->
<link href="res/css/font-awesome.css" rel="stylesheet" text="text/css" />
<!-- the css is dumped here -->
<style type="text/css">
<?php echo $board['css']; ?>
</style>
<script>
    $().ready(function(){
        var last_id = 0;

        var timer = $.timer(function() {
		$.post(
			'http://talmina.org/tagboard/poll',
			{
				'board': '<?php echo $board['identifier']; ?>',
				'last_id': last_id
			},
            null,
            'json'
		)
		.success(function(data) {
		    //append the stuff
		    console.log(data.last_id);
		    
            if(data.last_id != null && last_id != data.last_id)
		    {
    			//set the new last id
    			last_id = data.last_id;
    			$('.talmina_tb_body').append(data.post_text);
                if($('.talmina_tb_input_scroll').prop('checked'))
                    $('.talmina_tb_body').scrollTop($('.talmina_tb_body')[0].scrollHeight);
		    }
		    //console.log(data);
		})
		.error(function(data) {
		    alert("Sorry, the server reported an error.");
            console.log(data);
		});
            //console.log('This message was sent by the timer.');
        });

        //timer.once();
        timer.set({ time : 5000, autostart : true });
        //When the document is ready to go, do these things


        //when they click 'submit'
        $('#postit').click(function(){
            
            nick = $('.talmina_tb_input_nick').val();
            email = $('.talmina_tb_input_email').val();
            body = $('.talmina_tb_input_body').html();

            if(body != '')
            {
                $.post(
                    'http://talmina.org/tagboard/shout',
                    {
                        'board': '<?php echo $board['identifier']; ?>',
                        'nick': nick,
                        'email': email,
                        'body': body
                    }
                )
                .success(function(data) {
                    //do the time warp! (once, reset)
                    timer.reset();
                    timer.once();

                    //clear the input body (maybe add disable for this?)
                    $('.talmina_tb_input_body').html('');
                    //$('.talmina_tb_body').scrollTop($('.talmina_tb_body')[0].scrollHeight);
                })
                .error(function(data) {
                    alert("Sorry, the server reported an error.");
                    console.log(data);
                });
            }
        });
    });
</script>
</head>
<body>
<!-- We should wrap everything in a nice little package that is easy to handle -->
<div class="talmina_tb">
	<!-- the tagboard body. contains all posts -->
    <div class="talmina_tb_body">

    </div>
    <div class="talmina_tb_input_area">
        Nick <input type="text" class="talmina_tb_input_nick" />
        <div class="talmina_tbp_clear"></div>

        Email <input type="text" class="talmina_tb_input_email" />
        <div class="talmina_tbp_clear"></div>
        <div class="talmina_tb_kitchen_sink">
            <button title="Bold" rel="Bold" onclick="document.execCommand('bold', false, null);"><i class="icon-bold"></i></button>
            <button title="Italic" rel="Italic" onclick="document.execCommand ('italic', false, null);"><i class="icon-italic"></i></button>
            <button title="Underline" rel="Underline" onclick="document.execCommand('underline', false, null);"><i class="icon-underline"></i></button>
            <button title="Strkethrough" rel="Strikethrough" onclick="document.execCommand('strikeThrough', false, null);"><i class="icon-strikethrough"></i></button>

            <button title="Remove Formatting" rel="Remove Formatting" onclick="document.execCommand('removeFormat', false, 'null');"><i class="icon-ban-circle"></i></button>
        </div>
        <div class="talmina_tb_input_body" contenteditable="true"></div>
        <!--<textarea class="talmina_tb_input_body"></textarea>-->
        <input type="button" name="postit" id="postit" class="talmina_tb_input_submit" value="Submit" />
        <input type="checkbox" name="talmina_tb_input_scroll" class="talmina_tb_input_scroll" id="talmina_tb_input_scroll" checked /><label for="talmina_tb_input_scroll">Scroll on new posts?</label>
    </div>
</div>
</body>
</html><?php

//end 'board'
}
//the request doesn't fit any of our request types
else
{
    //not found header
    header('HTTP/1.0 404 Not Found');
    ?>
    <h1>Sorry</h1>
    That resource was not found.
    <?php
}
?>
